package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String arrChars, String word){
        boolean result;
        List<Character> arrCharsList = new ArrayList<>();
        List<Character> wordList = new ArrayList<>();
        List<Character> resultList = new ArrayList<>();
        String resultString = "";
        for(char nextIndex : arrChars.toLowerCase().toCharArray()) {
            arrCharsList.add(nextIndex);
        }
        for(char nextIndex : word.toLowerCase().toCharArray()) {
            wordList.add(nextIndex);
        }
        for (int i = 0; i < wordList.size(); i++) {
            for (int j = 0; j < arrCharsList.size(); j++) {
                if(wordList.get(i) == arrCharsList.get(j)){
                    resultList.add(arrCharsList.get(j));
                    arrCharsList.remove(j);
                    break;
                }
            }
        }
        for (int i = 0; i < resultList.size(); i++) {
            resultString = resultString + resultList.get(i);
        }
        if(resultString.equals(word.toLowerCase())){
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}
//END
