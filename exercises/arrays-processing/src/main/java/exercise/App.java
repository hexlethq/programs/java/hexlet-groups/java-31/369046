package exercise;

import java.util.Arrays;

class App {
    // BEGIN
        public static int getIndexOfMaxNegative(int [] array){

            if(array.length !=0) {
                int maxValue = array[0];
                int maxIndex = 0;
                int maxTemp = array[0];
                for (int i = 1; i < array.length; i++) {


                    if (array[i] < 0) {
                        maxValue = maxValue < array[i] ? array[i] : maxValue;
                        if (maxValue != maxTemp) {
                            maxTemp = maxValue;

                            maxIndex = i;
                        }
                    }
                }


                if(maxValue < 0){
                    return maxIndex;
                }

                return -1;
            }
            return -1;
        }


        public static int[] getElementsLessAverage(int[] arr){
            if(arr.length == 0){
                return arr;
            }

            int average = 0;
            int sum = 0;
            int counter = 0;

            for (int a : arr) {
                sum += a;
            }
            average = sum / arr.length;

            for (int j : arr) {
                if (j <= average) {
                    counter++;
                }
            }

            int[] arrNew = new int[counter];
            int j = 0;
            for (int i = 0; i < arr.length; i++) {
                if(arr[i] <= average){
                    arrNew[j] = arr[i];
                    j++;
                }
            }
                return arrNew;
        }

     //END
}
