package exercise;

class App {
    // BEGIN

    public static String getTypeOfTriangle(int aSide, int bSide, int cSide){

        if((aSide + bSide < cSide) || (aSide + cSide < bSide) || (bSide + cSide < aSide)){
            return "Треугольник не существует";
        }

        if(aSide == bSide && aSide == cSide){
            return "Равносторонний";
        }

        if(aSide != bSide && aSide != cSide && bSide != cSide){
            return "Разносторонний";
        }

        if(aSide == bSide || aSide == cSide || bSide == cSide){
            return "Равнобедренный";
        }

        return "Треугольник не существует";
    }



//    public static int getFinalGrade(int exam, int project){
//
//        if(exam > 90 || project > 10){
//            return 100;
//        }
//        else if(exam > 75 || project >=5){
//            return 90;
//        }
//        else if(exam > 50 || project >=2){
//            return 75;
//        }
//        else
//            return 0;
//
//    }
    // END
}

