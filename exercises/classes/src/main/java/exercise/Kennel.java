package exercise;

import java.util.Arrays;


// BEGIN

public class Kennel {
    public static String[][] puppies = new String[0][0];
    public static int puppiesCounter = 0;

    public static void addPuppy(String[] puppy) {
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[puppies.length - 1] = puppy;
        puppiesCounter++;
    }

    public static void addSomePuppies(String[][] puppiesArray) {
        puppies = Arrays.copyOf(puppies, puppies.length + puppiesArray.length);

        for (String[] puppy : puppiesArray) {
            puppies[puppiesCounter] = puppy;
            puppiesCounter++;
        }
    }

    public static int getPuppiesCounter() {
        return puppiesCounter;
    }

    public static Boolean isContainPuppy(String puppiesName) {
        for (int i = 0; i < puppies.length; i++) {
            if (puppiesName.equals(puppies[i][0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        int puppiesCounter = 0;
        for (int i = 0; i < puppies.length; i++) {
            if (breed.equals(puppies[i][1])) {
                puppiesCounter++;
            }
        }
        String[] name = new String[puppiesCounter];
        int resultName = 0;
        for (int i = 0; i < puppies.length; i++) {
            if (breed.equals(puppies[i][1])) {
                name[resultName] = puppies[i][0];
                resultName++;
            }
        }
        return name;
    }

    public static void resetKennel() {
        puppies = new String[0][0];
        puppiesCounter = 0;
    }
}


// END
