// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] point1, double[] point2) {
        double[][] segment = new double[2][2];
        segment[0][0] = Point.getX(point1);
        segment[0][1] = Point.getY(point1);
        segment[1][0] = Point.getX(point2);
        segment[1][1] = Point.getY(point2);
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint;
        beginPoint = Point.makePoint(segment[0][0], segment[0][1]);
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endPoint;
        endPoint = Point.makePoint(segment[1][0], segment[1][1]);
        return endPoint;
    }
}

// END
