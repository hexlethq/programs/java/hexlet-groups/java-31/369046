package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str){
        String conc = String.valueOf(str.charAt(0));
        char probel = ' ';

        for (int i = 0; i < str.length()-1; i++){
            if(str.charAt(i) == probel){
                conc = conc.trim().concat(String.valueOf(str.charAt(i+1)));
            }
        }
        return conc.toUpperCase();
    }
    // END
//    public static void main(String[] args) {
//        System.out.println(getAbbreviation("     Проверяем        код на абривиатуры   "));
//    }
}
