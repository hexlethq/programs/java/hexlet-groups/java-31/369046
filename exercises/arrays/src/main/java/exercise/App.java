package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] array) {
        if(array.length == 0){
            return array;
        }
        int [] copyArray = Arrays.copyOf(array, array.length);

        for (int i = 0; i < copyArray.length / 2; i++) {
            int temp = copyArray[i];
            copyArray[i] = copyArray[copyArray.length - 1 - i];
            copyArray[copyArray.length - 1 - i] = temp;
        }
            return copyArray;
    }

    public static int mult(int [] array){
        if(array.length == 0){
            return 1;
        }
        int sum = 1;
        for (int i = 0; i < array.length ; i++) {
            sum = array[i] * sum;
        }
        return sum;
    }
    // END
}
