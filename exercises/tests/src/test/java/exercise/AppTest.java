package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> list_Test = Arrays.asList(9, 8, 7, 6, 5, 4, 3);
        int count_Test = 5;
        List<Integer> expected = Arrays.asList(9, 8, 7, 6, 5);
        assertThat(expected).isEqualTo(App.take (list_Test, count_Test));
        // END
    }
}
