package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
class App {
    public static long getCountOfFreeEmails(List<String> emails)  {
        if (emails.size() == 0) {
            return -1;
        }

        return emails.stream()
                .filter(mail -> mail.contains("@gmail.com") ||
                        mail.contains("@yandex.ru") ||
                        mail.contains("@hotmail.com"))
                .count();
    }
}
// END
