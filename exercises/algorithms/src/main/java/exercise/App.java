package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] array){
        int temp = 0;
        for(int i = array.length; i > 0; i--){
            for(int j = 0; j < array.length-1; j++ ){
                if(array[j] > array[j+1]){
                    temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }

//    public static void main(String[] args) {
//        int [] test = {3, 5, -3, 9, 10, -8};
//        System.out.println(Arrays.toString(sort(test)));
//    }
    // END
}
