package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

class App {
    // BEGIN
    public static String buildList(String[] array){
        if(array.length == 0){
            return "";
        }
        //StringBuilder liBuilder = new StringBuilder();

        StringBuilder result = new StringBuilder("<ul>\n");
        for (String s : array) {
            result.append("  <li>").append(s).append("</li>\n");
        }
        //result.append("</ul>"); //result = result.append("</ul>");
        return result.append("</ul>").toString();
    }

    static public String getUsersByYear(String [][] usersArr, int year){

        StringBuilder result = new StringBuilder("<ul>\n");
        int counter = 0;
        for (int i = 0; i < usersArr.length; i++) {
            LocalDate date = LocalDate.parse(usersArr[i][1]);
            int yearR = date.getYear();
                if(yearR == year){
                    result.append("  <li>").append(usersArr[i][0]).append("</li>\n");
                    counter++;
            }
        }
        if(counter != 0) {
            return result.append("</ul>").toString();
        }
        else return "";
    }

//    public static void main(String[] args) {
//        String[] test = {"apples", "pears", "bananas", "apples1", "pears1", "bananas1", "apples2", "pears2", "bananas2"};
//        String[][] users = {
//                {"Andrey Petrov", "1990-11-23"},
//                {"Aleksey Ivanov", "1995-02-15"},
//                {"Anna Sidorova", "1996-09-09"},
//                {"John Smith", "1990-03-11"},
//                {"Vanessa Vulf", "1985-11-16"},
//                {"Vladimir Nikolaev", "1990-12-27"},
//                {"Alice Lucas", "1986-01-01"},
//                {"Elsa Oscar", "2000-03-25"},
//        };
//        //System.out.println(buildList(test));
//        System.out.println(getUsersByYear(users, 1986));
//    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
//    public static String getYoungestUser(String[][] users, String date) throws Exception {
//        // BEGIN
//
//        // END
//    }

}
