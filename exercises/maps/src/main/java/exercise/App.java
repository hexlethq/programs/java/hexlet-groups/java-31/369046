package exercise;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// BEGIN
class App {
    public static Map<String, Integer> getWordCount(String stringForParsing) {
        Map<String, Integer> wordsCount = new HashMap<>();
        if(stringForParsing.trim().equals("")) {
            return wordsCount;
        }
        String[] keyValuePairs = stringForParsing.split(" ");

        for (String car : keyValuePairs) {
            if(wordsCount.containsKey(car)) {
                wordsCount.put(car, wordsCount.get(car) + 1);
            } else {
                wordsCount.put(car, 1);
            }
        }
        return wordsCount;
    }

    public static String toString(Map<String, Integer> mapForString) {
        String resultFirstCurve = "{";
        String resultSecondCurve = "}";
        String temp = "";
        if(mapForString.isEmpty()){
            return resultFirstCurve + resultSecondCurve;
        }
        for(Map.Entry<String, Integer> counter: mapForString.entrySet()){
            temp = temp +  "\n" + "  " + counter.getKey() + ":" + " " + counter.getValue();
        }
        return resultFirstCurve + temp + "\n" + resultSecondCurve;
    }

//    public static void main(String[] args) {   // Test method
//        String sentence1 = "";
//        System.out.println(toString(getWordCount(sentence1)));
//    }
}
//END
