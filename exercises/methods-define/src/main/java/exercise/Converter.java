package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    public static int convert(int convInt, String size){
        if(size.equals("b")){
            return convInt * 1024;
        }
        else if (size.equals("Kb")){
            return convInt / 1024;
        }
        else
            return 0;
    }
    // END
}