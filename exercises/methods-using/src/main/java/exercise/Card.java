package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String symb ="";
        for(int i = 1; i <= starsCount; i ++){
            symb = symb.concat("*");
        }
        System.out.println(symb.concat(cardNumber.substring(12)));
        // END
    }
}
