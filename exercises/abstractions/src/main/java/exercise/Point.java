package exercise;

import java.util.Arrays;
import java.util.Random;

class Point {
    // BEGIN

    public static int[] makePoint(int x, int y){
        int [] point = {x, y};
        return point;
    }
    public static int getX(int[] pointX){
        return pointX[0];
    }
    public static int getY(int[] pointY){
        return pointY[1];
    }
    public static String pointToString(int[] point){
        int x = point[0];
        int y = point[1];
        return "("+ x + "," + " " +y + ")";
    }
    public static int getQuadrant(int[] pointOfQuadrant){
        int x = pointOfQuadrant[0];
        int y = pointOfQuadrant[1];
        if(x > 0 && y > 0){
            return 1;
        }
        if(x < 0 && y > 0){
            return 2;
        }
        if(x < 0 && y < 0){
            return 3;
        }
        if(x > 0 && y < 0){
            return 4;
        }

        return 0;
    }
    // END
//    public static void main(String[] args) {
//        int[] point = {4, -5};
//        System.out.println(pointToString(point));
//    }
}
